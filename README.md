# SAST-with-Semgrep

## Possible Demo Projects
https://github.com/snoopysecurity/Vulnerable-Code-Snippets/blob/master/SQL%20Injection/example.java
[Pet-Clinic](https://github.com/spring-projects/spring-petclinic)
[Vuln Pet-Clinic](https://github.com/lampska/spring-petclinic)

## Getting started

1. Fork [Vuln Pet Clinic](https://github.com/universalamateur/spring-petclinic-vuln.git) into Gitlab
2. Security and Complliance -> Security Configuration
3. Enable Sast
4. Exlcude ", .mvn, gradle"
5. Create merge request
6. Show security report
7. Merge
8. Got to vulnerability report
9. Create Issue out of one Vulnerability
10. Create Merge Request out of issue
11. Make changes in Merge request
12. Check piepeline
13. Mark MR as ready
14. Merge
15. Got To vulnerability report

## Changes to Remidiate in src/main/java/org/springframework/samples/petclinic/owner/OwnerRepositoryCustomImpl.java

![image.png](./image.png)

### Search for vulnerabilities in the Semgrep rules

The rule repository is located in [GitLab.org - security-productssecurity-products - analyzers - semgrep - Repository](https://gitlab.com/gitlab-org/security-products/analyzers/semgrep/-/tree/main/rules)

Open a Gitpod there or clone the repo localy, then you can find the pattern easily by grepping for the description, for example:
`grep -HiRn -e 'SQL.INJECTION' .`
